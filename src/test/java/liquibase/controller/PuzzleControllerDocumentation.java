package liquibase.controller;


import liquibase.Application;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Documentation and test
 * {@see http://docs.spring.io/spring-restdocs/docs/current/reference/html5/#documenting-your-api}
 *
 * @author Roman Uholnikov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class PuzzleControllerDocumentation {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .alwaysDo(document("{method-name}/{step}/"))
                .build();
    }


    @Test
    public void index() throws Exception {
        this.mockMvc.perform(get("/").accept(MediaTypes.HAL_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.answers", is(notNullValue())))
                .andExpect(jsonPath("_links.puzzles", is(notNullValue())))
                .andExpect(jsonPath("_links.profile", is(notNullValue())))
                .andExpect(jsonPath("_links.persons", is(notNullValue())));
    }


    @Test
    public void start() throws Exception {
        this.mockMvc.perform(get("/start").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("index"));
    }

    @Test
    public void isRightAnswerTrue() throws Exception {
        this.mockMvc.perform(get("/isRightAnswer?puzzleId=4&answer=A%20towel").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andDo(document("is-right-answer-true", requestParameters(
                        parameterWithName("puzzleId").description("The id of the puzzle"),
                        parameterWithName("answer").description("Text value of answer")
                )));

    }

    @Test
    public void isRightAnswerFalse() throws Exception {
        this.mockMvc.perform(get("/isRightAnswer?puzzleId=4&answer=wrong-answer").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andDo(document("is-right-answer-false"));
    }

}