package liquibase.service;

import liquibase.domain.entity.Puzzle;
import liquibase.repository.AnswerRepository;
import liquibase.repository.PersonRepository;
import liquibase.repository.PuzzleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


/**
 * @author r.uholnikov
 */
@Service
public class AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private PuzzleRepository puzzleRepository;

    @Autowired
    private PersonRepository personRepository;

    public boolean isRightAnswer(Long puzzleId, String answer) {

        Puzzle puzzle = puzzleRepository.findOne(puzzleId);
        String originalAnswer = puzzle != null ? puzzle.getAnswer() : null;
        return isRightAnswer(answer, originalAnswer);
    }

    public boolean isRightAnswer(String answer, String originalAnswer) {
        String trimedAnswer = originalAnswer.toLowerCase()
                .replace("the ","")
                .replace("a ","")
                .replace(".", "");
        if (StringUtils.isEmpty(answer))
            return false;
        if (answer.length() > trimedAnswer.length() / 2) {
            return trimedAnswer.trim().contains(answer.toLowerCase().trim());
        }
        return false;
    }


}
