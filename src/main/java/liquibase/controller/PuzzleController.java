package liquibase.controller;

import liquibase.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author r.uholnikov
 */
@Controller
public class PuzzleController {

    @Autowired
    private AnswerService answerService;

    /**
     * Entry point in the application.
     * @return start page
     */
    @ResponseBody
    @RequestMapping(value = "/start")
    public String start() {
        return "hello world";
    }

    /**
     * Check validity of provided answer. It is quite rough check, do not expect a lot from it.
     * @param puzzleId if of puzzle
     * @param answer string representation of answer
     * @return true - if answer considered to be correct, or false otherwise
     */
    @ResponseBody
    @RequestMapping(value = "/isRightAnswer")
    public String isRightAnswer(@Param("puzzleId") Long puzzleId, @Param("answer") String answer) {
        return String.valueOf(answerService.isRightAnswer(puzzleId, answer));
    }
}
