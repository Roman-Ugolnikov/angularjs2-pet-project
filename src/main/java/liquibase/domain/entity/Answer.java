package liquibase.domain.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author r.uholnikov
 */
@Entity
@Table(name = "answer")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @JoinColumn(name="person")
    @ManyToOne(targetEntity = Person.class, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Person person;

    @JoinColumn(name="puzzle")
    @ManyToOne(targetEntity = Puzzle.class, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Puzzle puzzle;

    private String answer;

    private boolean fail;

    @Column(name="timestamp")
    private LocalDateTime dateTime;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getSystemUser() {
        return person;
    }

    public void setSystemUser(Person person) {
        this.person = person;
    }

    public Puzzle getPuzzle() {
        return puzzle;
    }

    public void setPuzzle(Puzzle puzzle) {
        this.puzzle = puzzle;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isFail() {
        return fail;
    }

    public void setFail(boolean fail) {
        this.fail = fail;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getDateFormated(){
        return dateTime.toString();
    }
}
