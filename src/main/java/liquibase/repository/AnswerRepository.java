package liquibase.repository;

import liquibase.domain.entity.Answer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface AnswerRepository extends PagingAndSortingRepository<Answer, Long> {
    List<Answer> findByPerson(@Param("personId") Long personId);
    List<Answer> findByPuzzle(@Param("puzzleId") Long puzzleId);
    List<Answer> findByPersonIdAndFail(@Param("personId") Long personId,@Param("fail") Boolean fail);
}
