package liquibase.repository;

import liquibase.domain.entity.Person;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {
    /**
     * In order to have urls like
     * <pre>
     *     http://localhost:8889/persons/search/findByTribe?tribe=people
     * </pre>
     * @param tribe full name of tribe
     * @return
     */
    List<Person> findByTribe(@Param("tribe") String tribe);

    List<Person> findByGender(@Param("gender") String gender);

    /**
     * In order to have urls like
     * <pre>
     *   http://localhost:8889/persons/search/findByNameContains?name=Ari
     * </pre>
     *
     * @param name part of the name
     * @return
     */
    List<Person> findByNameContains(@Param("name") String name);
}
