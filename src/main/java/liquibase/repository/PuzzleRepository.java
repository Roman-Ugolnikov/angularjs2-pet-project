package liquibase.repository;

import liquibase.domain.entity.Puzzle;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PuzzleRepository extends PagingAndSortingRepository<Puzzle, Long> {

}
